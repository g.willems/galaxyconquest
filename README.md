**Galaxy Conquest** is a clone of the KDE game
[Konquest](https://apps.kde.org/konquest/).


How to Play
-----------

The Konquest [handbook](https://docs.kde.org/stable5/en/konquest/konquest/index.html)
is a good introduction to the rules.

There are however a few differences:
* there is no "kill percentage" here, all ships will arrive at destination
* neutral planets don't produce ships
* the combat rules are slightly different
* the current AI players aren't very smart...


Run from the Sources
---------------------

No installation required, just run in a terminal:

```
python3 -B -m galaxyconquest
```


Contribute
----------

Please follow the [GNOME Code of Conduct](https://conduct.gnome.org/).


Disclaimer
----------

THIS SOFTWARE IS PROVIDED "AS IS", WITH ABSOLUTELY NO WARRANTY, REAL
OR IMPLIED.
