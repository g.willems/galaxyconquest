#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random


MAX_PLAYERS = 8

PLAYER_NAMES = "Alice Arnold Betty Bob Carol Chuck Dave Debra Elvis Emily Flora Floyd Gina Greg Harry Helen"

PLAYER_COLORS = (
    (1.0, 0.0, 0.0),     # red
    (0.0, 0.0, 1.0),     # blue
    (0.0, 1.0, 0.0),     # green
    (1.0, 1.0, 0.0),     # yellow
    (1.0, 0.0, 1.0),     # pink
    (0.0, 1.0, 1.0),     # cyan
    (1.0, 0.625, 0.0),   # orange
    (0.625, 0.0, 1.0),   # purple
    (0.0, 0.0, 0.0)      # black
)


def get_new_player_name(game):
    """Return a name which first char matches the planet."""
    c = chr(ord('A') + len(game.players))
    return random.choice([n for n in _(PLAYER_NAMES).split() if n.startswith(c) and n not in (pr.name for pr in game.players)])


def get_next_color(game):
    """Return next unassigned color."""
    return [c for c in PLAYER_COLORS if c not in (pr.color for pr in game.players)][0]

