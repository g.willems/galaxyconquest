#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

from gi.repository import GLib, Gio, Gtk


ACTIONS = [
    # Section, InMenu, [Group, Name+target, Type, Description, Enable, Shortcuts]
    (_("Setup"), True, [
        ('win',    'newgame',           None, _("_New Game"),             False, ['<Control>n']),
    ]),
    (_("Gameplay"), False, [
        (None,     'selectplanet',      None, _("_Select Planet"),        False, ['a...z']),
        ('win',    'addship(1)',        'i',  _("_Increase Ships Count"), False, ['plus', 'KP_Add']),
        ('win',    'addship(-1)',       'i',  _("_Decrease Ships Count"), False, ['minus', 'KP_Subtract']),
        ('win',    'addfleet',          None, _("_Send Fleet"),           False, ['asterisk', 'KP_Multiply']),
        ('win',    'undo',              None, _("_Undo Fleet"),           False, ['<Control>z']),
        ('win',    'endturn',           None, _("_End Turn"),             False, ['slash', 'KP_Divide']),
    ]),
    (_("General"), True, [
        ('win',    'help',              None, _("_Help"),                 True,  ['F1']),
        ('win',    'show-help-overlay', None, _("_Keyboard Shortcuts"),   True,  ['<Control>question']),
        ('win',    'about',             None, _("_About"),                True,  []),
        ('window', 'close',             None, _("_Close Window"),         True,  ['<Control>w', '<Control>q']),
    ]),
]


def setup_shortcuts(app):
    """Setup accelerators for application."""
    for title, in_menu, shortcuts in ACTIONS:
        for g, a, t, d, e, k in shortcuts:
            if g:
                app.set_accels_for_action(f'{g}.{a}', k)


def setup_actions(window, obj):
    """Setup actions for window."""
    scwin = Gtk.ShortcutsWindow(section_name='shortcuts')
    scsec = Gtk.ShortcutsSection(section_name='shortcuts')
    scwin.add_section(scsec)
    window.set_help_overlay(scwin)
    menu = Gio.Menu()
    for title, in_menu, shortcuts in ACTIONS:
        scgrp = Gtk.ShortcutsGroup(title=title)
        submenu = Gio.Menu()
        for g, a, t, d, e, k in shortcuts:
            if d:
                if g == 'win':
                    submenu.append(d, f'{g}.{a}')
                if k:
                    d2 = d.replace("_", "")
                    scgrp.add_shortcut(Gtk.ShortcutsShortcut(title=d2, accelerator=k[0]))
            ok, n, v = Gio.Action.parse_detailed_name(a)
            if g != 'win' or window.has_action(n):
                continue
            action = Gio.SimpleAction.new(n, GLib.VariantType(t) if t else None)
            action.set_enabled(e)
            action.connect('activate', getattr(obj, f'on_action_{n}'))
            window.add_action(action)
        submenu.freeze()
        if in_menu and submenu.get_n_items() > 0:
            menu.append_section(None, submenu)
        scsec.add_group(scgrp)
    menu.freeze()
    return menu

