#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

from gi.repository import Gdk, Gtk, Graphene


class ColorFactory(Gtk.SignalListItemFactory):
    """Custom ListItemFactory for list of colors."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__()
        self.connect('setup', self.on_setup)
        self.connect('bind', self.on_bind)

    def on_setup(self, factory, listitem):
        """Callback on list item setup."""
        listitem.set_child(ColorWidget())

    def on_bind(self, factory, listitem):
        """Callback on list item bind."""
        item = listitem.get_item()
        w = listitem.get_child()
        w.set_color(item.get_string())


class ColorWidget(Gtk.Widget):
    """Custom widget to display a color."""
    __gtype_name__ = __qualname__

    def __init__(self):
        super().__init__(width_request=40, height_request=20, overflow='hidden')
        self.color = Gdk.RGBA()

    def set_color(self, color_spec):
        """Set color from description."""
        self.color.parse(color_spec)
        self.queue_draw()

    def do_snapshot(self, snap):   # override
        """Drawing handler."""
        w, h = self.get_width(), self.get_height()
        rect = Graphene.Rect().init(0.0, 0.0, w, h)
        snap.append_color(self.color, rect)


ColorWidget.set_css_name('colorwidget')

