#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import builtins
import os.path

from gi.repository import Gio, Gdk, Gtk, Adw

from .actions import setup_shortcuts
from .mainwin import GalaxyConquestUI


class GalaxyConquestApp(Adw.Application):
    """Application GalaxyConquest"""
    __gtype_name__ = __qualname__

    def __init__(self, game):
        super().__init__(application_id='org.gnome.gitlab.gwillems.GalaxyConquest')
        self.set_flags(self.get_flags() | Gio.ApplicationFlags.NON_UNIQUE)
        self.game = game
        self.gui = None
        setup_shortcuts(self)
        self.connect('startup', self.on_startup)
        self.connect('activate', self.on_activate)

    def on_startup(self, app):
        """Application startup."""
        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        cssp = Gtk.CssProvider()
        cssp.load_from_path(os.path.join(MODULE_PATH, "galaxyconquest.css"))
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), cssp, priority)

    def on_activate(self, app):
        """Show window on activation."""
        if self.gui is None:
            self.gui = GalaxyConquestUI(app, self.game)
            builtins.game_log = self.gui.add_to_backlog
        self.gui.window.present()

