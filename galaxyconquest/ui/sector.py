#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random
from math import pi as PI

from gi.repository import GLib, Gtk, Gdk, cairo


class PlanetDrawing():
    """Draw a planet on a Cairo context."""

    def __init__(self):
        self.planet = None
        self.planetonly = True
        self.has_rings = True
        self.has_satellite = False

    def redraw(self, ctx, size):
        """Draw the sector on the given Cairo context."""
        txt_margin = size / 20.0
        ctx.set_line_join(cairo.LineJoin.ROUND)
        ctx.set_line_cap(cairo.LineCap.ROUND)
        ctx.select_font_face("Sans", cairo.FontSlant.NORMAL, cairo.FontWeight.BOLD)
        if not self.planetonly and self.planet is None:
            return
        # Player color
        if self.planetonly or self.planet is None or self.planet.owner is None:
            pR, pG, pB = 0.5, 0.5, 0.5
        else:
            pR, pG, pB = (0.75*_c for _c in self.planet.owner.color)
        # Planet
        ctx.save()
        ctx.translate(size/2.0, size/2.0)
        ctx.set_source_rgb(pR, pG, pB)
        ctx.arc(0.0, 0.0, 0.3*size, 0.0, 2.0*PI)
        ctx.fill()
        if self.planetonly:
            ctx.set_source_rgb(0.0, 0.0, 0.0)
        else:
            ctx.set_source_rgb(1.0, 1.0, 1.0)
        ctx.set_line_width(size/50.0)
        ctx.arc(0.0, 0.0, 0.3*size, 0.0, 2.0*PI)
        ctx.stroke()
        if self.has_rings:
            ctx.rotate(-PI/12.0)
            ctx.scale(1.0, 0.3)
            ctx.set_line_width(size/10.0)
            ctx.arc_negative(0.0, 0.0, 0.425*size, 4.8*PI/4.0, 7.2*PI/4.0)
            ctx.stroke()
        elif self.has_satellite:
            ctx.translate(0.3*size, 0.1*size)
            ctx.set_source_rgb(0.3, 0.3, 0.3)
            ctx.arc(0.0, 0.0, 0.12*size, 0.0, 2.0*PI)
            ctx.fill()
            ctx.set_source_rgb(1.0, 1.0, 1.0)
            ctx.set_line_width(size/75.0)
            ctx.arc(0.0, 0.0, 0.12*size, 0.0, 2.0*PI)
            ctx.stroke()
        ctx.restore()
        if not self.planetonly:
            # Name
            ctx.set_source_rgb(0.6, 0.6, 0.6)
            ctx.set_font_size(size/5.0)
            (xb, yb, w, h, xa, ya) = ctx.text_extents(self.planet.name)
            ctx.move_to(round(txt_margin), round(txt_margin - yb))
            ctx.show_text(self.planet.name)
            # Spaceships
            ctx.set_source_rgb(0.6, 0.6, 0.6)
            ctx.set_font_size(size/5.0)
            (xb, yb, w, h, xa, ya) = ctx.text_extents(str(self.planet.spaceships))
            ctx.move_to(round(size - txt_margin-xa), round(size - txt_margin))
            ctx.show_text(str(self.planet.spaceships))


class Sector(Gtk.DrawingArea, PlanetDrawing):
    """Grid sector, containing a planet or space void."""
    __gtype_name__ = __qualname__

    def __init__(self, clic_cb):
        Gtk.DrawingArea.__init__(self)
        PlanetDrawing.__init__(self)
        self.set_content_height(24)
        self.set_content_width(24)
        self.set_draw_func(self.draw_func)
        self.planetonly = False
        self.reset(None)
        ev_click = Gtk.GestureClick(button=Gdk.BUTTON_PRIMARY)
        ev_click.connect('pressed', lambda *_a: clic_cb(self.planet))
        self.add_controller(ev_click)

    def reset(self, planet):
        """Assign a planet (or None) to the sector."""
        self.planet = planet
        self.has_rings = random.choice([True, False])
        self.has_satellite = random.choice([True, False])

    def update(self):
        """Update planet properties and trigger redraw."""
        if self.planet is not None:
            tt = "<b>" + GLib.markup_escape_text(_("Planet {}").format(self.planet.name)) + "</b>"
            tt += "\n" + GLib.markup_escape_text(_("Production = {}").format(self.planet.prod_rate))
            if self.planet.owner is not None:
                tt += "\n" + GLib.markup_escape_text(_("Player = {}").format(self.planet.owner.name))
            tt += "\n" + GLib.markup_escape_text(_("Ships = {}").format(self.planet.spaceships))
            self.set_tooltip_markup(tt)
        else:
            self.set_tooltip_markup(None)
        self.queue_draw()

    def draw_func(self, da, ctx, w, h):
        """Override virtual draw handler."""
        size = min(w, h)
        self.redraw(ctx, size)



### python3 -B -m ui.sector "galaxyconquest.svg" 256
if __name__ == '__main__':
    import sys
    import cairo as cairo_   # not same as gi.repository.cairo!
    assert len(sys.argv) == 3
    size = 0.8 * int(sys.argv[2])
    svg = cairo_.SVGSurface(sys.argv[1], size, size)
    ctx = cairo_.Context(svg)
    pd = PlanetDrawing()
    pd.redraw(ctx, size)
    svg.show_page()
    svg.finish()
    svg.flush()

