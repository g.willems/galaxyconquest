#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import os.path

from gi.repository import GLib, Pango, Gio, Gdk, Gtk, Adw

from .actions import setup_actions
from .listitems import ColorFactory
from .sector import Sector
from . import ui_data


class GalaxyConquestUI():
    """User interface of GalaxyConquest"""

    def __init__(self, app, game):
        self.game = game
        self.mainplayer = None
        # Defaults
        self.prefs_name      = GLib.get_user_name() if GLib.get_real_name() == "Unknown" else GLib.get_real_name()
        self.prefs_grid_size = 10
        self.prefs_n_planets = 10
        self.prefs_n_players = 1
        self.prefs_n_ships   = 10
        self.prefs_brain     = ""
        self.prefs_color     = None
        # GUI
        self.dialogs = set()
        self.builder = Gtk.Builder(self)
        self.builder.add_from_file(os.path.join(MODULE_PATH, "galaxyconquest.ui"))
        self.window = self.builder.get_object('window_main')
        self.window.set_application(app)
        # Actions
        menu = setup_actions(self.window, self)
        self.builder.get_object('button_menu').set_menu_model(menu)
        # Planets grid
        self.grid = self.builder.get_object('grid_planets')
        self.__rebuild_grid()
        # Play interface
        self.src = self.builder.get_object('combobox_src')
        self.dst = self.builder.get_object('combobox_dst')
        self.nships = self.builder.get_object('spinbutton_nships')
        # Fleets viewer
        self.__init_fleet_view()
        # Backlog
        self.backlog = self.builder.get_object('textview_backlog').get_buffer()
        # Prefs
        self.builder.get_object('entry_playername').set_text(self.prefs_name)
        self.builder.get_object('spinbutton_gridsize').set_value(self.prefs_grid_size)
        self.builder.get_object('spinbutton_nplanets').set_value(self.prefs_n_planets)
        self.builder.get_object('spinbutton_nai').get_adjustment().set_upper(ui_data.MAX_PLAYERS - 1)
        self.builder.get_object('spinbutton_nai').set_value(self.prefs_n_players)
        self.builder.get_object('combobox_tai').get_model().append(_("???"))
        for b in self.game.pmgr.get_ai_brains():
            self.builder.get_object('combobox_tai').get_model().append(b)
        self.builder.get_object('combobox_tai').set_selected(0)
        self.builder.get_object('combobox_color').set_factory(ColorFactory())
        for c in ui_data.PLAYER_COLORS:
            self.builder.get_object('combobox_color').get_model().append(self.__get_hex_color(c))
        self.builder.get_object('combobox_color').set_selected(0)
        self.show_settings()

    def __rebuild_grid(self):
        """Recreate empty grid sectors when starting a new game."""
        for c in list(self.grid):
            self.grid.remove(c)
        for i in range(self.prefs_grid_size):
            for j in range(self.prefs_grid_size):
                w = Sector(self.on_sector_clicked)
                self.grid.attach(w, i, j, 1, 1)

    def __init_fleet_view(self):
        """Configure the fleets list view."""
        tree_fleets = self.builder.get_object('treeview_fleets')
        # ListStore: src, dst, ships, arrival, arrival%, fontweight
        self.list_fleets = Gtk.ListStore(str, str, str, str, int, int)
        tree_fleets.set_model(self.list_fleets)
        c1 = Gtk.TreeViewColumn(_("From"), Gtk.CellRendererText(), text=0, weight=5)
        c1.set_expand(False)
        c1.set_sort_column_id(0)
        tree_fleets.append_column(c1)
        c2 = Gtk.TreeViewColumn(_("To"), Gtk.CellRendererText(), text=1, weight=5)
        c2.set_expand(False)
        c2.set_sort_column_id(1)
        tree_fleets.append_column(c2)
        c3 = Gtk.TreeViewColumn(_("Ships"), Gtk.CellRendererText(), text=2, weight=5)
        c3.set_expand(False)
        c3.set_sort_column_id(2)
        tree_fleets.append_column(c3)
        c4 = Gtk.TreeViewColumn(_("Arrival in turns"), Gtk.CellRendererProgress(), text=3, value=4)
        ##c4.get_property('cell-area').foreach(lambda w: w.set_property('inverted', True))
        c4.set_expand(True)
        c4.set_sort_column_id(4)
        tree_fleets.append_column(c4)

    def __get_hex_color(self, rgb):
        """Return Pango hex color from RGB color array."""
        return '#{:02x}{:02x}{:02x}'.format(*(int(255 * _c) for _c in rgb))

    def refresh(self):
        """Update the GUI when fleets list modified, or at end of turn."""
        self.builder.get_object('window_title').set_title(_("Turn {}").format(self.game.turn))
        self.builder.get_object('window_title').set_subtitle("")
        for c in self.grid:
            c.update()
        self.list_fleets.clear()
        for f in self.mainplayer.fleets:
            progress = 100 * f.arrival / (1.5 * self.prefs_grid_size)   # max arrival = sqrt(2)*grid_size
            weight = Pango.Weight.BOLD if f.start_turn == self.game.turn else Pango.Weight.NORMAL
            self.list_fleets.append([f.planet_src.name, f.planet_dst.name, str(f.spaceships), str(f.arrival), progress, weight])

    def add_to_backlog(self, txt, planet, player, ships, restricted):
        """Write status to the backlog."""
        if restricted and self.mainplayer is not player:
            return
        ptcolor = '#BFBFBF' if planet is None or planet.owner is None else self.__get_hex_color(planet.owner.color)
        fmt = {}
        fmt['pt'] = "" if planet is None else f'<span underline="low" underline_color="{ptcolor}"> {GLib.markup_escape_text(planet.name)} </span>'
        fmt['pr'] = "" if player is None else f'<span underline="single" underline_color="{self.__get_hex_color(player.color)}">{GLib.markup_escape_text(player.name)}</span>'
        fmt['sp'] = ships
        if planet is None and player is None:
            txt = f"\n\n<b>{GLib.markup_escape_text(txt)}</b>".format_map(fmt)
        else:
            txt = f"\n{GLib.markup_escape_text(txt)}".format_map(fmt)
        self.backlog.insert_markup(self.backlog.get_end_iter(), txt, -1)
        self.on_backlog_resized(self.builder.get_object('textview_backlog'))

    def read_prefs(self):
        """Get preferences from GUI."""
        self.builder.get_object('spinbutton_gridsize').update()
        self.builder.get_object('spinbutton_nplanets').update()
        self.builder.get_object('spinbutton_nai').update()
        self.prefs_name      = self.builder.get_object('entry_playername').get_text()
        self.prefs_grid_size = int(self.builder.get_object('spinbutton_gridsize').get_value())
        self.prefs_n_planets = int(self.builder.get_object('spinbutton_nplanets').get_value())
        self.prefs_n_players = int(self.builder.get_object('spinbutton_nai').get_value())
        self.prefs_brain     = self.builder.get_object('combobox_tai').get_selected_item().get_string() if self.builder.get_object('combobox_tai').get_selected() > 0 else None
        self.prefs_color     = ui_data.PLAYER_COLORS[self.builder.get_object('combobox_color').get_selected()]

    def show_settings(self):
        """Show game settings page."""
        if not self.game.end:
            self.game.end_game()
        self.builder.get_object('window_title').set_title(_("Settings"))
        self.builder.get_object('window_title').set_subtitle("")
        self.builder.get_object('stack_playerinput').set_visible_child_name('stackpage_settings')
        self.window.lookup_action('newgame').set_enabled(False)
        self.window.lookup_action('addship').set_enabled(False)
        self.window.lookup_action('addfleet').set_enabled(False)
        self.window.lookup_action('undo').set_enabled(False)
        self.window.lookup_action('endturn').set_enabled(False)

    def new_game(self):
        """Setup a new game."""
        self.read_prefs()
        assert self.prefs_name != ""
        assert self.prefs_n_players + 1 <= self.prefs_n_planets
        self.game.new_game(self.prefs_grid_size, self.prefs_n_planets, self.prefs_n_ships)
        self.game.add_player("Human", self.prefs_name, self.prefs_color)
        for p in range(self.prefs_n_players):
            self.game.add_player(self.prefs_brain, ui_data.get_new_player_name(self.game), ui_data.get_next_color(self.game))
        self.mainplayer = self.game.players[0]
        self.__rebuild_grid()
        for c in self.grid:
            x, y, nc, nr = self.grid.query_child(c)
            c.reset(self.game.get_planet_at(x, y))
        self.src.set_selected(0)
        self.dst.set_selected(0)
        src_model = self.src.get_model()
        dst_model = self.dst.get_model()
        src_model.splice(1, src_model.get_n_items()-1, [_p.name for _p in self.game.planets])
        dst_model.splice(1, dst_model.get_n_items()-1, [_p.name for _p in self.game.planets])
        self.backlog.set_text(_("New game started"))
        self.window.lookup_action('newgame').set_enabled(True)
        self.window.lookup_action('addship').set_enabled(True)
        self.window.lookup_action('endturn').set_enabled(True)
        self.builder.get_object('stack_playerinput').set_visible_child_name('stackpage_game')
        self.refresh()

    def set_transient_style(self, widget, style_class, timeout=2000, /):
        """Apply style, then revert after timeout."""
        widget.add_css_class(style_class)
        GLib.timeout_add(timeout, widget.remove_css_class, style_class)

    def set_selection_style(self, p_src, p_dst):
        """Calculate distance when another planet selected."""
        for s in self.grid:
            if p_src is not None and s.planet is p_src:
                s.add_css_class('fleet-src')
            else:
                s.remove_css_class('fleet-src')
            if p_dst is not None and s.planet is p_dst:
                s.add_css_class('fleet-dst')
            else:
                s.remove_css_class('fleet-dst')

    def ask(self, question, cbk):
        """Show popup dialog to ask for confirmation."""
        assert callable(cbk)
        d = Adw.AlertDialog.new(question, None)
        d.add_response('yes', _("Yes"))
        d.add_response('cancel', _("Cancel"))
        d.set_response_appearance('yes', Adw.ResponseAppearance.DESTRUCTIVE)
        d.choose(self.window, None, self.on_dialog_response, cbk)
        self.dialogs.add(d)   # keep reference

    def on_dialog_response(self, dialog, res, cbk):
        """Callback on response choice."""
        try:
            r = dialog.choose_finish(res)
        except Exception:
            r = ''
        self.dialogs.discard(dialog)
        if r == 'yes':
            GLib.idle_add(cbk)

    def on_expand(self, w):
        """Make fleets/backlog expanders shrink/expand their content."""
        w.set_vexpand(not w.get_expanded())

    def on_backlog_resized(self, w):
        """Auto scroll to end of backlog."""
        adj = w.get_vadjustment()
        GLib.idle_add(lambda: adj.set_value(adj.get_upper()))

    def on_key_press(self, event, keyval, keycode, state):
        """Callback on key press event."""
        if not self.game.end and state == 0:   # no modifiers
            c = chr(keyval)
            if 'a' <= c <= 'z':
                pt = self.game.get_planet_by_name(c.upper())
                self.on_sector_clicked(pt)
                return Gdk.EVENT_STOP
        return Gdk.EVENT_PROPAGATE

    def on_sector_clicked(self, pt):
        """Update planet name and distance when clicking on a sector."""
        if pt is None:
            self.builder.get_object('window_title').set_subtitle("")
            return
        index = 1 + ord(pt.name) - ord('A')
        if pt.owner is not self.mainplayer:
            self.dst.set_selected(index)
        elif self.src.get_selected() <= 0:
            self.src.set_selected(index)
        elif self.dst.get_selected() <= 0:
            self.dst.set_selected(index)
        else:
            self.src.set_selected(index)
            self.dst.set_selected(0)
        self.builder.get_object('window_title').set_subtitle(pt.get_description())

    def on_start_clicked(self, w):
        """Callback on start game clicked."""
        self.new_game()

    def on_action_newgame(self, action, param):
        """New game request."""
        if self.game.end:
            self.show_settings()
        else:
            self.ask(_("Abort current game?"), self.show_settings)

    def on_adj_changed(self, w):
        """Callback on settings adjustements changed."""
        adj_grid = self.builder.get_object('adjustment_gridsize')
        adj_ais = self.builder.get_object('adjustment_nai')
        adj_planets = self.builder.get_object('adjustment_nplanets')
        adj_planets.set_lower(1 + adj_ais.get_value())
        adj_planets.set_upper(min(26, adj_grid.get_value()**2))
        adj_planets.set_value(adj_planets.get_value())   # auto-clamp

    def on_action_help(self, action, param):
        """Show the help."""
        Gtk.show_uri(self.window, 'help:galaxyconquest', Gdk.CURRENT_TIME)

    def on_action_about(self, action, param):
        """Show about dialog."""
        d = Adw.AboutDialog()
        d.set_application_name(GLib.get_application_name())
        d.set_application_icon(self.window.get_icon_name())
        d.present(self.window)

    def on_close_request(self, w):
        """Quit the game."""
        if not self.game.end:
            self.ask(_("Really quit?"), lambda: w.destroy())
            return True
        return False

    def on_action_addship(self, action, param):
        """Add ships to the current fleet."""
        if param is not None:
            i = param.unpack()
            self.nships.set_value(i + int(self.nships.get_value()))

    def on_action_addfleet(self, action, param):
        """Launch a new fleet."""
        p_src = self.game.get_planet_by_name(self.src.get_selected_item().get_string())
        p_dst = self.game.get_planet_by_name(self.dst.get_selected_item().get_string())
        self.nships.update()
        s = int(self.nships.get_value())
        if p_src is None or p_src.owner is not self.mainplayer:
            self.set_transient_style(self.src, 'error')
        elif p_dst is None:
            self.set_transient_style(self.dst, 'error')
        elif 0 < s <= p_src.spaceships:
            self.mainplayer.send_fleet(p_src, p_dst, s)
            self.window.lookup_action('undo').set_enabled(True)
            self.refresh()
        else:
            self.set_transient_style(self.nships, 'error')

    def on_action_undo(self, action, param):
        """Cancel last fleet, if turn not over."""
        if self.mainplayer is not None:
            r = self.mainplayer.cancel_fleet()
            self.window.lookup_action('undo').set_enabled(r)
            self.refresh()

    def on_action_endturn(self, action, param):
        """End of turn."""
        if not self.game.end:
            self.game.play_turn()
            self.window.lookup_action('undo').set_enabled(False)
            if self.game.end:
                self.nships.set_sensitive(False)
                self.window.lookup_action('addfleet').set_enabled(False)
                self.window.lookup_action('endturn').set_enabled(False)
            self.refresh()

    def on_planet_changed(self, obj, pspec):
        """Calculate distance when another planet selected."""
        p_src = self.game.get_planet_by_name(self.src.get_selected_item().get_string())
        p_dst = self.game.get_planet_by_name(self.dst.get_selected_item().get_string())
        self.set_selection_style(p_src, p_dst)
        dist = ""
        sens_nship, sens_add = False, False
        if p_src is not None:
            if p_src.owner is self.mainplayer:
                self.nships.update()
                self.nships.get_adjustment().set_upper(p_src.spaceships)
                self.nships.set_value(min(int(self.nships.get_value()), p_src.spaceships))
                sens_nship = not self.game.end
            if p_dst is not None:
                dist = _("in {} turns").format(p_src.get_distance_from(p_dst))
                if p_src.owner is self.mainplayer:
                    sens_add = not self.game.end
        self.builder.get_object('label_distance').set_text(dist)
        self.nships.set_sensitive(sens_nship)
        self.window.lookup_action('addfleet').set_enabled(sens_add)

