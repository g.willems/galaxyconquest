#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random

from ..core.player import Player


class AIPlayerSimple(Player):
    """Simple A.I. player: sends random number of ships to random planet when owned planet exceeds ships limit."""

    BRAIN = _("Simple A.I.")

    def play(self):
        """Simple A.I. play function."""
        for pt in self.planets:
            if pt.spaceships > 20:
                dst = random.choice(self.get_all_planets())
                if pt is dst:
                    continue
                s = random.randint(10, pt.spaceships-10)
                self.send_fleet(pt, dst, s)

