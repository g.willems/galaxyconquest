#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random

from ..core.player import Player


class AIPlayerBalanced(Player):
    """Balanced A.I. player: better attack, safer defense."""

    BRAIN = _("Balanced A.I.")

    def get_safe_limit(self, my_pt):
        """Ensure enough defensive ships against neighbors."""
        lim = self.game.turn - len(self.planets)
        for pt in self.get_all_planets():
            if pt.owner is self:
                continue
            lim = max(lim, pt.spaceships - my_pt.prod_rate * my_pt.get_distance_from(pt))
        return max(lim, 0)

    def get_attack_fleet(self, my_pt, pt):
        """Check how many ships are required to takeover a planet."""
        attack = pt.spaceships + pt.prod_rate * my_pt.get_distance_from(pt) * (0 if pt.owner is None else 1)
        return attack + self.game.turn

    def play(self):
        """Balanced A.I. play function."""
        for my_pt in self.planets:
            while True:
                targets = [_pt for _pt in self.get_all_planets() if _pt.owner is not self and my_pt.spaceships - self.get_attack_fleet(my_pt, _pt) >= self.get_safe_limit(my_pt)]
                if len(targets) == 0:
                    targets = [_pt for _pt in self.get_all_planets() if _pt.owner is not self or _pt.spaceships < self.get_safe_limit(_pt)]
                    if len(targets) > 0 and my_pt.spaceships > 10 + self.get_safe_limit(my_pt):
                        dst = random.choice(targets)
                        s = random.randint(10, my_pt.spaceships - self.get_safe_limit(my_pt))
                    else:
                        break
                else:
                    dst = random.choice(targets)
                    s = self.get_attack_fleet(my_pt, dst)
                self.send_fleet(my_pt, dst, s)

