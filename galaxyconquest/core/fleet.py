#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0


class Fleet():
    """Fleet of spaceships."""

    def __init__(self, player, src, dst, n_ships, start_turn):
        self.player = player
        self.planet_src = src
        self.planet_dst = dst
        self.spaceships = n_ships
        self.start_turn = start_turn
        self.arrival = max(src.get_distance_from(dst), 1)

    def play_turn(self):
        """Update the fleet status for this turn."""
        assert self.arrival > 0
        self.arrival -= 1

    def is_arrived(self):
        """True if the fleet reached the destination."""
        return self.arrival <= 0

