#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random

from .planet import Planet
from .player import AIManager


class GalaxyConquestGame():
    """GalaxyConquest game logic."""

    def __init__(self):
        self.pmgr = AIManager()
        self.turn = 0
        self.init_ships = 0
        self.players = []
        self.planets = []
        self.end = True

    def new_game(self, size, n_planets, n_ships):
        """Initialize a new game (no players)."""
        assert 1 <= n_planets <= ord('Z') - ord('A') + 1
        assert n_ships > 0
        assert n_planets <= size**2
        self.init_ships = n_ships
        self.players = []
        self.planets = []
        while len(self.planets) < n_planets:
            x = random.randrange(0, size)
            y = random.randrange(0, size)
            if self.get_planet_at(x, y) is None:
                name = chr(ord('A') + len(self.planets))
                self.planets.append(Planet(name, x, y))
        self.end = False
        self.turn = 1

    def add_player(self, brain, name, color):
        """Add new player to the game."""
        assert len(self.players) < len(self.planets)
        prclass = self.pmgr.get_player_class(brain)
        pr = prclass(self, name, color)
        pr.add_planet(self.planets[len(self.players)], self.init_ships)
        self.players.append(pr)

    def get_planet_at(self, x, y):
        """Get planet at coordinates."""
        for p in self.planets:
            if p.x == x and p.y == y:
                return p
        return None

    def get_planet_by_name(self, n):
        """Get planet by name."""
        for p in self.planets:
            if p.name == n:
                return p
        return None

    def play_turn(self):
        """Compute next game status."""
        game_log(_("Turn {}").format(self.turn), None, None, 0, False)
        for pr in self.players:
            pr.play_turn()
        for pt in self.planets:
            pt.play_turn()
        alive_players = [pr for pr in self.players if not pr.check_dead()]
        if len(alive_players) < 2:
            self.end_game(alive_players.pop() if alive_players else None)
        self.turn += 1

    def end_game(self, winner=None):
        """End the game."""
        if winner is not None:
            game_log(_("End of the game, {} is the winner!").format(winner.name), None, None, 0, False)
        else:
            game_log(_("End of the game, no winner."), None, None, 0, False)
        self.end = True


### MAIN for testing purpose ###
### python3 -B -m galaxyconquest.core.game
if __name__ == '__main__':
    import builtins
    import sys
    builtins._ = lambda _txt: _txt
    builtins.game_log = lambda _txt,_pt,_pr,_sp,_r: print(_txt.format(pt="" if _pt is None else _pt.name, pr="" if _pr is None else _pr.name, sp=_sp))
    random.seed()
    gc_game = GalaxyConquestGame()
    gc_game.new_game(10, 10, 10)
    gc_game.add_player(None, "Alice", (1.0, 0.0, 0.0))
    gc_game.add_player(None, "Bob",   (0.0, 0.0, 1.0))
    gc_game.add_player(None, "Chuck", (0.0, 1.0, 0.0))
    for t_pr in gc_game.players:
        print(t_pr.name, f"({t_pr.BRAIN})", sep="\t")
    while '--noinput' in sys.argv or input() != "q":
        for t_pt in gc_game.planets:
            print(t_pt.name, t_pt.spaceships, "-" if t_pt.owner is None else t_pt.owner.name, sep="\t")
        gc_game.play_turn()
        if gc_game.turn > 10_000:
            gc_game.end_game()
        if gc_game.end:
            sys.exit(0)

