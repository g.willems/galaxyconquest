#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import importlib
import os.path
import random

from .fleet import Fleet

AI_MODULES = {}


class Player():
    """Player sends fleets of spaceships to conquer planets."""

    BRAIN = None

    def __init__(self, g, name, color):
        self.name = name
        self.color = color
        self.game = g
        self.planets = []
        self.fleets = []
        self.is_dead = False

    def add_planet(self, p, n_ships):
        """Player captured a planet."""
        self.planets.append(p)
        p.set_owner(self, n_ships)

    def remove_planet(self, planet):
        """Player lost a planet."""
        self.planets.remove(planet)
        planet.set_owner(None, 0)

    def get_all_planets(self):
        """List all planets on the board."""
        return self.game.planets

    def send_fleet(self, src, dst, s):
        """Send a fleet of spaceships."""
        assert src.spaceships >= s
        f = Fleet(self, src, dst, s, self.game.turn)
        self.fleets.append(f)
        f.planet_src.spaceships -= f.spaceships

    def cancel_fleet(self):
        """Cancel last sent fleet."""
        if self.__can_cancel_fleet(self.game.turn):
            fl = self.fleets.pop()
            fl.planet_src.spaceships += fl.spaceships
        return self.__can_cancel_fleet(self.game.turn)

    def __can_cancel_fleet(self, turn):
        """Check if last sent fleet is cancelable."""
        if len(self.fleets) > 0:
            if self.fleets[-1].start_turn == turn:
                return True
        return False

    def get_ships_count(self):
        """Get total number of owned spaceships."""
        return sum(_p.spaceships for _p in self.planets) + sum(_f.spaceships for _f in self.fleets)

    def check_dead(self):
        """Check if player still alive."""
        if not self.is_dead:
            if len(self.planets) == 0 and len(self.fleets) == 0:
                self.is_dead = True
                game_log(_("Player {pr} has lost!"), None, self, 0, False)
        return self.is_dead

    def play_turn(self):
        """Update the player status for this turn."""
        if self.is_dead:
            return
        self.play()
        for f in list(self.fleets):
            f.play_turn()
            if f.is_arrived():
                f.planet_dst.fleet_arrived(f)
                self.fleets.remove(f)

    def play(self):
        pass


class HumanPlayer(Player):
    """Human player. No play function, as fleets were already sent using the GUI."""

    BRAIN = _("Human")


class AIManager():
    """Discover and maintain a list of available AIs."""

    def __init__(self):
        global AI_MODULES
        self.brains = {}
        p = os.path.join(MODULE_PATH, "ai")
        for m in (f for f in os.listdir(p) if os.path.isfile(os.path.join(p, f)) and f.lower().endswith(".py")):
            m = m[:-3]
            AI_MODULES[m] = importlib.import_module(f"..ai.{m}", __spec__.parent)
        for a in Player.__subclasses__():
            self.brains[a.BRAIN] = a

    def get_player_class(self, name):
        """Return AI object from name."""
        if name is None:
            name = random.choice(self.get_ai_brains())
        if name in self.brains:
            return self.brains[name]
        else:
            return None

    def get_ai_brains(self):
        """Get list of AIs."""
        l = sorted(list(self.brains.keys()))
        l.remove(HumanPlayer.BRAIN)
        return l

