#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import math
import random


class Planet():
    """Planet can be captured by a player and produces spaceships."""

    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y
        self.prod_rate = random.randint(5, 15)
        self.owner = None
        self.spaceships = random.randint(5, 9)
        self.fleets = []

    def get_distance_from(self, p):
        """Get the distance to another planet."""
        return math.floor(math.sqrt((self.x-p.x)**2 + (self.y-p.y)**2))

    def get_description(self):
        """Get planet description."""
        fmt = {'pt': self.name, 'sp': self.prod_rate}
        if self.owner is not None:
            fmt['pr'] = self.owner.name
            fmt['br'] = self.owner.BRAIN
            desc = _("Planet {pt}: produces {sp} ships\u202f/\u202fturn, owned by {pr} ({br})")
        else:
            desc = _("Planet {pt}: produces {sp} ships\u202f/\u202fturn")
        return desc.format_map(fmt)

    def set_owner(self, player, n_ships):
        """Set the owner of the planet."""
        self.owner = player
        self.spaceships = n_ships

    def fleet_arrived(self, fleet):
        """Notify that a fleet arrived."""
        self.fleets.append(fleet)

    def change_owner(self, new_owner, n_ships):
        """Change the owner of the planet."""
        if self.owner is not None:
            self.owner.remove_planet(self)
        if new_owner is not None:
            new_owner.add_planet(self, n_ships)

    def solve_owner(self):
        """If fleets arrived, check who wins the fight."""
        forces_in_place = {self.owner: self.spaceships}
        for f in self.fleets:
            if f.player not in forces_in_place:
                forces_in_place[f.player] = 0
            if f.player is self.owner:
                game_log(_("Planet {pt} received reinforcements ({sp} ships)."), self, f.player, f.spaceships, True)
            forces_in_place[f.player] += f.spaceships
        while len(forces_in_place) > 1:
            killed = set()
            # Each player's ship targets randomly one opponent's ship
            # A player won't target several times the same ship, but different players may target the same ship
            for pr in forces_in_place:
                targets = [(_k, _v) for _k in forces_in_place for _v in range(forces_in_place[_k]) if _k is not pr]
                killed.update(random.sample(targets, min(len(targets), forces_in_place[pr])))
            # All ships fire at the same time, targeted ships are immediately destroyed
            for pr in forces_in_place:
                forces_in_place[pr] -= len([_k for _k in killed if _k[0] is pr])
            # Check who is still there, start a new round if more than one survivor
            for pr in list(forces_in_place.keys()):
                if forces_in_place[pr] <= 0:
                    del forces_in_place[pr]
                    if pr is not self.owner:
                        game_log(_("Planet {pt} has held against an attack from {pr}."), self, pr, 0, False)
        if len(forces_in_place) == 0:
            self.spaceships = 0
        else:
            (pr, s) = forces_in_place.popitem()
            if pr is self.owner:
                self.spaceships = s
            else:
                game_log(_("Planet {pt} has fallen to {pr}."), self, pr, 0, False)
                self.change_owner(pr, s)
        self.fleets = []

    def play_turn(self):
        """Update the planet status for this turn."""
        if self.owner is not None:
            self.spaceships += self.prod_rate
        self.solve_owner()

