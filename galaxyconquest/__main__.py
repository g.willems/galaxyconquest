#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import random
import sys

from gi.repository import GLib

from .core.game import GalaxyConquestGame
from .ui.app import GalaxyConquestApp


def galaxyconquest_run():
    random.seed()
    GLib.set_prgname("galaxyconquest")
    GLib.set_application_name(_("Galaxy Conquest"))
    game = GalaxyConquestGame()
    app = GalaxyConquestApp(game)
    return app.run(sys.argv)


if __name__ == '__main__':
    sys.exit(galaxyconquest_run())

