#!/usr/bin/env python3
# SPDX-License-Identifier: CC0-1.0

import builtins
import gettext
import sys

import gi


# Check minimal Python version
assert sys.version_info >= (3, 10)

# Introspection deps
if hasattr(gi, 'disable_legacy_autoinit'):
    gi.disable_legacy_autoinit()
gi.require_version('Pango', '1.0')
gi.require_version('Gdk', '4.0')
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

# Misc builtins
builtins.MODULE_PATH = __path__[0]

# Translations
gettext.install('galaxyconquest')

